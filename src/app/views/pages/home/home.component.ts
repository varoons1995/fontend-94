import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { PusherService } from 'src/app/pusher.service';
import { MatDialog } from'@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { environment } from '../../../../environments/environment';
import axios from 'axios'
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'kt-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  displaybankowner: string[] = ['ID','BANK_TITLE','BANK_ACCOUNT_NUMBER','SUM_TODAY','CREATEDATE'];
  displayedhidden: string[] = ['TRANSACTION_DEPOSIT_ID','CREATEDATE','BALANCE',];
  displaytrxde_new: string[] = ['MEMBER_USERNAME','TRANSACTION_GEAR','ACTION_BY','BALANCE','CREATEDATE'];
    token = localStorage.getItem('Token')
    profile =  JSON.parse(localStorage.getItem('Profile'));
  // bank owner
    bankonwer : any;
    trxde_new : any;
    
    day = new Date().getDate()+1
    setDate = new Date().setDate(this.day)
    date = new Date(this.setDate)
    creatDate = new FormControl(new Date());
    endDate = new FormControl(new Date(this.date));

    api=environment.apibackend;

  // table right release
    trxright;


  // data new top/hidden
    trxhidden

  

  constructor(public dialog: MatDialog,
    private pusherService: PusherService,
    private changeDetectorRefs: ChangeDetectorRef,
    private store: Store<AppState>,){
      
  }

  ngOnInit() {
    this.pusherService.channel.bind('my-event', data => {
     // alert(data.message);
      if(data.message == 'refresh'){
        this.preData();
        this.changeDetectorRefs.detectChanges();  
      }

    });
    console.log(this.profile)
    this.preData();
    this.changeDetectorRefs.detectChanges();  
  }


  preData(){
    let get_frist_date = this.creatDate.value;
    let frist_date = ("0" + get_frist_date.getDate()).slice(-2);
    let frist_month = ("0" + (get_frist_date.getMonth() + 1)).slice(-2);
    let frist_year = get_frist_date.getFullYear();
    let f_date = frist_year + "-" + frist_month + "-" + frist_date;

    let get_last_date = this.endDate.value;
    let last_date = ("0" + get_last_date.getDate()).slice(-2);
    let last_month = ("0" + (get_last_date.getMonth() + 1)).slice(-2);
    let last_year = get_last_date.getFullYear();
    let l_date = last_year + "-" + last_month + "-" + last_date;
    axios({
      method: 'post',
      url: this.api+'/home',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data:{
        "CREATEDATE":f_date,
        "ENDDATE":l_date
      }
    })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
        console.log("response: ", response.data)
        // bankonwer
        this.bankonwer = response.data.bank_owner
        this.trxright = response.data.trxde_order_desc
        this.trxhidden = response.data.trxde_new
        // do something about response
        this.changeDetectorRefs.detectChanges();  
        }
      })
      .catch(err => {
        console.error(err)
    
      })
  }

  openDialog(id) {
    console.log(id)
    const dialogRef = this.dialog.open(HomeDailogTopup, {
      height: '37%',
      width:'45',
      data :{
        dataKey : id
      }
      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.preData();
      this.changeDetectorRefs.detectChanges();
    });
  }

  openDialogHidden(id) {
    console.log(id)
    const dialogRef = this.dialog.open(HomeDailogHidden, {
      height: '37%',
      width:'45',
      data :{
        dataKey : id
      }
      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.preData();
      this.changeDetectorRefs.detectChanges();
    });
  }

  // chcekData(){
  //   this.profile =
  // }


  /** Gets the total cost of all transactions. */
  getTotalAmount() {
    const bankonwer = this.bankonwer ? this.bankonwer.map(t => t.SUM_TODAY).reduce((acc, value) => acc + value, 0) : null
    return bankonwer
  }

}



@Component({
  selector:'home-dialog-topup',
  templateUrl:'home-dialog-topup.html'
})
  export class HomeDailogTopup {

    constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public matDialog: MatDialog ,
      private store: Store<AppState>
   ) { 

   }
   id='';
   token = localStorage.getItem('Token')
   profile =  JSON.parse(localStorage.getItem('Profile'));
   dataModal;
   sbb;
   submitButton = false;
   api = environment.apibackend;
   ngOnInit() {
    console.log(this.data)
    this.dataModal = this.data.dataKey
    console.log(this.dataModal)
   }

   onSearch(){
    axios({
			method: 'get',
			url: this.api+'/member/'+this.id,
			headers: {
				'Authorization': 'Bearer '+this.token
			  },
		  })
			.then(response => {
        console.log(response.data)
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
          if(response.data != ''){
            alert('Username ลูกค้าทำรายการได้')
            this.submitButton = true;
            this.sbb = true;
          }else{
            alert('ไม่พบ Username ของลูกค้า กรุณาตรวจสอบ Username ขอลูกค้า')
          }
        }
        
      })
      .catch(err => {
        console.error(err)
       // this.store.dispatch(new Logout());
      })
   }

   onSubmit(){
     console.log(this.id)
     if(this.id !=''){
      let dd = parseFloat(this.dataModal.BALANCE)
      if(this.profile.DEPOSITLIMIT >= dd){
        this.submitButton = false;
    axios({
      method: 'post',
      url: this.api+'/deposit/edit',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data:{
        "MEMBER_USERNAME":this.id,
        "BALANCE":this.dataModal.BALANCE,
        "TRANSACTION_DEPOSIT_ID":this.dataModal.TRANSACTION_DEPOSIT_ID,
        "ACTION":"Add",
        "ACTION_BY":this.profile.STAFFNAME,
        "REMARK":""
      }
    })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
        console.log("response: ", response.data)
        if(response.data.message == 'Success'){
          this.matDialog.closeAll();
        }else{
          alert('ไม่สามารถเติมเงินให้ลูกค้า')
          this.matDialog.closeAll();
        }
      }
      })
      .catch(err => {
        console.error(err)
       // this.store.dispatch(new Logout());
      })
      }else{
        alert('ยอดฝากเกินกำหนดสิทธิของ Staff')
      }
    }else{
      alert('กรุณาใส่ Username ลูกค้า')
    }
   }

  }


  @Component({
    selector:'home-dialog-hidden',
    templateUrl:'home-dialog-hidden.html'
  })
    export class HomeDailogHidden {
  
      constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public matDialog: MatDialog,
        private store: Store<AppState>
     ) { 
  
     }
     remark='';
     dataModal;
     api = environment.apibackend;
     token = localStorage.getItem('Token')
     profile =  JSON.parse(localStorage.getItem('Profile'));
     ngOnInit() {
      console.log(this.data)
      this.dataModal = this.data.dataKey
      console.log(this.dataModal)
     }
  
     onSubmit(){
       console.log(this.remark)
       if(this.remark !=''){
      axios({
        method: 'post',
        url: this.api+'/deposit/edit',
        headers: {
          'Authorization': 'Bearer '+this.token
        },
        data:{
          "TRANSACTION_DEPOSIT_ID":this.dataModal.TRANSACTION_DEPOSIT_ID,
          "ACTION":"Hide",
          "ACTION_BY":this.profile.STAFFNAME,
          "REMARK":this.remark
        }
      })
        .then(response => {
          console.log("response: ", response.data)
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
            if(response.data.message == 'Success'){
              this.matDialog.closeAll();
            }else{
              alert('ไม่สามารถอัพเดทได้')
              this.matDialog.closeAll();
            }
          }
        })
        .catch(err => {
          console.error(err)
          // this.store.dispatch(new Logout());
        })
      }else{
        alert('กรุณาใส่รายละเอียด')
      }
     }
  
    }