import { Component, OnInit, AfterViewInit, Inject ,ChangeDetectorRef} from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import axios from 'axios'
import { HttpClient } from '@angular/common/http';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { environment } from '../../../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';

@Component({
  selector: 'kt-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})

export class StaffComponent implements OnInit  {
  token = localStorage.getItem('Token')
    profile =  JSON.parse(localStorage.getItem('Profile'));
  displayedColumnsddd: string[] = ['ID','USERNAME','STAFFNAME','CLASSNAME' ,'DEPOSITLIMIT' ,'ACTIVE' ,'STATUS_ONLINE'];
  tableSource;
  results;
  loading = true;
  api=environment.apibackend;

  constructor(public dialog: MatDialog,private http: HttpClient ,
    private changeDetectorRefs: ChangeDetectorRef,
    private store: Store<AppState>,) {
  }
  
  openDialog(index) {
    const dialogRef = this.dialog.open(DialogStaff, {
      height: '500px',
      data: {
        dataKey: this.tableSource[index]
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.onTest();
    });
  }

  ngOnInit(){
     this.onTest();
    }

    async onTest(){
      console.log('Ontest')
       await axios({
          method: 'get',
          // headers: {
          //   'Authorization': `bearer ${token}` 
          // },
          url: this.api+'/staff/all',
          headers: {
            'Authorization': 'Bearer '+this.token
          },
        })
          .then(response => {
            if(response.data.status ==401){
              alert('มีการล็อคอินใหม่')
              this.store.dispatch(new Logout());
            }else{
            console.log("response: ", response.data)
            // do something about response
          this.tableSource = response.data
          this.changeDetectorRefs.detectChanges();
            }
          })
          .catch(err => {
            console.error(err)
            this.store.dispatch(new Logout());
          })
    }

    // await this.http.get('http://127.0.0.1:3333/staff/all').subscribe(data => {
    //     // อ่านค่า result จาก JSON response ที่ส่งออกมา
    //     this.tableSource = data
    //     console.log(this.tableSource)
    // });    

    

}

@Component({
  selector:'dialog-staff',
  templateUrl:'dialog-staff.html',
  styleUrls: ['./staff.component.scss']
})
  export class DialogStaff implements OnInit {
    token = localStorage.getItem('Token')
    profile =  JSON.parse(localStorage.getItem('Profile'));
    api=environment.apibackend;
    constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public matDialog: MatDialog,
      private store: Store<AppState>
   ) { 

   }

    Staffdata;
    StaffClassAll;

    ngOnInit(){
      axios({
        method: 'get',
        url: this.api+'/staffclass/all',
        headers: {
          'Authorization': 'Bearer '+this.token
        },
        })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
          // do something about response
          this.StaffClassAll = response.data
          }
        })
        .catch(err => {
          console.error(err)
          this.store.dispatch(new Logout());
        })
      console.log(this.data.dataKey)
      this.Staffdata = this.data.dataKey
      console.log(this.Staffdata)      
    }
    async onSubmit(){
      console.log(this.Staffdata)
      await axios({
        method: 'post',
        url: this.api+'/staff/edit',
        headers: {
          'Authorization': 'Bearer '+this.token
        },
        data:{
          'STAFFNAME':this.Staffdata.STAFFNAME,
          'DEPOSITLIMIT':this.Staffdata.DEPOSITLIMIT,
          'STAFFCLASSID':this.Staffdata.STAFFCLASSID,
          'STATUS':this.Staffdata.ACTIVE,
          'ID':this.Staffdata.ID,
        }
      })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
          console.log("response: ", response.data)
          // do something about response
          if(response.data.message == "Suscess"){
            alert('แก้ไขเรียบร้อย')
            this.matDialog.closeAll();
          }else{
            alert('ไม่สามารถทำรายการได้กรุณารอสักครู่ แล้วทำรายการใหม่')
            this.matDialog.closeAll();
          }
        }
        }) 
        .catch(err => {
          console.error(err)
          this.store.dispatch(new Logout());
        })
      }
  }
